#!/usr/bin/env python2
# Author: Volynets I.A.
# date  : 2016-08

# Dependecies: matplotlib  (pip install matplotlib)

# Calculation of delays and 16-bit timer top values of ATMega uC
# for smooth moving of stepper (with constant acceleration).
# Part of constants are contained in file "const":
#	DRIVER_MICRO_STEP	- driver microsteps
#	MOTOR_STEPS		- number of steps in full rotation
#	
# File f_out will contains results in C header format

# List of references:
# [1] Aryeh Eiderman <leib@eiderman.com>, Real Time Stepper Motor Linear Ramping Just by Addition and Multiplication 

from math import sqrt, pi
import matplotlib.pyplot as plt


number_of_steps = 3200 	# how many steps stepper will accelerate to max speed
speed_max_rpm   = 200	# final speed

ATMEGA_CPU_FREQ 		= 16e6 * 1.0
ATMEGA_timer_prescaler 	= 8/8		# prescaler of ATMEGA 16-bit timer
ATMEGA_timer_max		= 2**16-1	# max of 16-bit timer
ATMEGA_timer_min		= 1
rad2rpm					= 9.55		

MOTOR_STEPS_IN_ROTATION	= {	'x': 200, 
							'y': 200, 
							'z': 200}
							
DRIVER_MICRO_STEP 		= {	'x': 16,
							'y': 16,
							'z': 16}
							
MOTOR_MM_IN_ROTATION 	= {	'x': 60.92, # mm/rotation
							'y': 60.92, # mm/rotation
							'z': 40.63}  # mm/rotation

class Motor(object):
	
	def __init__(self, name):
		print "Creating motor instance..."
		self.name 						= name
		self.rotation_in_mm				= MOTOR_MM_IN_ROTATION[self.name]
		self.microstep					= DRIVER_MICRO_STEP[self.name]			# driver  parameter
		self.motor_steps_in_rotation	= MOTOR_STEPS_IN_ROTATION[self.name]	# stepper parameter 
	
	def create_pulses(self, number_of_delays, speed_max_rpm):	
		self.step 			= range(number_of_delays+1)
		self.distance 		= [0 for i in range(number_of_delays+1)]
		self.delay_time_us 	= [0 for i in range(number_of_delays+1)]
		self.speed_in_step 	= [0 for i in range(number_of_delays+1)]
		self.speed_rpm 		= [0 for i in range(number_of_delays+1)]
		self.delay_v2 		= [0 for i in range(number_of_delays+1)]
		self.accel 			= [0 for i in range(number_of_delays+1)]
		self.time 			= [0]
		self.atmega_timer_top = [0 for i in range(number_of_delays+1)]
		sum = 0
		
		self.steps_in_rotation 	= 1.0 * self.microstep * self.motor_steps_in_rotation	# driver and stepper parameter 			
		self.alpha 				= 1.0 * 2*pi/self.steps_in_rotation						# rad/step
		self.speed_in_step_max 	= 1.0 * speed_max_rpm/60*self.steps_in_rotation
		self.accel_in_step 		= 1.0 * self.speed_in_step_max**2/(2*number_of_delays) 	# constant acceleration
		self.accel_in_rad  		= 1.0 * self.accel_in_step * self.alpha	
		pulses_accel = []
		for i in range(1, number_of_delays+1):
			self.speed_in_step[i] 	= 1.0 * sqrt(self.speed_in_step[i-1]**2 + 2.0*self.accel_in_step) 	# see [1, formula 6], steps/sec
			self.speed_rpm [i] 		= 1.0 * self.speed_in_step[i] * self.alpha * rad2rpm			# rad/sec
			self.delay_time_us[i] 	= 1.0 * 1e6*(self.speed_in_step[i]-self.speed_in_step[i-1])/self.accel_in_step	# micro seconds
			self.distance[i] 		= 1.0 * (self.speed_in_step[i]**2 - self.speed_in_step[0]**2)/(2.0*self.accel_in_step) 
			
			sum = sum + 1.0 * self.delay_time_us[i]/1e6
			self.time.append(sum)
			self.accel[i] = 1.0 * (self.speed_in_step[i]-self.speed_in_step[i-1])/(self.delay_time_us[i]/1e6)
			self.atmega_timer_top[i] = int(round(self.delay_time_us[i] * ATMEGA_CPU_FREQ/1e6/ATMEGA_timer_prescaler - 1))
			if self.atmega_timer_top[i]>ATMEGA_timer_max-1 or self.atmega_timer_top[i]<ATMEGA_timer_min:
				print("Error in timer value! %s. Replace with ATMEGA_timer_max = %s" % (self.atmega_timer_top[i], ATMEGA_timer_max))
				self.atmega_timer_top[i] = ATMEGA_timer_max
				#exit(0)
			print("%s:\tdelay=%5.2f us\tATMEGA_timer=%d\tspeed_in_step=%5.2f\tspeed_rpm=%.2f\taccel=%.2f" \
					%(i, self.delay_time_us[i], self.atmega_timer_top[i], self.speed_in_step[i], self.speed_rpm[i], self.accel[i]))
		
		
		
		self.ACCEL_IN_PULSES = number_of_delays


#print("number_of_delays=%s\t speed_in_step_max=%s\t accel=%s" %(number_of_delays, speed_in_step_max, accel_in_step))



if __name__ == "__main__":
	f_out = "./accel_delays.h"
	f = open(f_out	, 'w')
	
	# motors: x, y or z
	m = Motor('x')
	
	
	number_of_delays=number_of_steps-1
	m.create_pulses(number_of_delays, speed_max_rpm)
	print("Total time: %s sec" % round(m.time[-1], 4))
	print("Max speed: %s steps/sec or %s RPM" % (round(m.speed_in_step[-1], 4), round(m.speed_rpm[-1], 4)))
	print("Max speed: %s mm/sec" % (speed_max_rpm*m.rotation_in_mm/60))
	print("Min delay: %s us, Max delay: %s us"  % (round(m.delay_time_us[-1], 4), (round(m.delay_time_us[1], 4))))
	f.write("#define SPEED_MAX_PPM %d\n" % (speed_max_rpm))
	f.write("#define STEPS_NUMBER %d\n" % (number_of_delays+1))
	f.write("#define DELAYS_NUMBER (STEPS_NUMBER-1)\n")
	f.write("const PROGMEM  uint16_t charSet[DELAYS_NUMBER] = {\n")
	#f.write("%s,\n" %str(m.atmega_timer_top[1]))
	for i in range(1, len(m.atmega_timer_top), 20):
		f.write("%s,\n" %str(m.atmega_timer_top[i:i+20])[1:-1])
	f.write("};\n")
	
	f.close()
	
	plt.figure(1)
	plt.subplot(221)
	plt.ylabel('Delay, us')
	plt.xlabel('Step')
	plt.plot(m.step, m.delay_time_us, 'b-o')

	plt.subplot(222)
	plt.plot(m.time, m.speed_in_step, 'g-o')
	plt.ylabel('Speed, Steps/Sec')
	plt.xlabel('Time')

	plt.subplot(223)
	plt.plot(m.time, m.distance, 'r-o')
	plt.ylabel('Distance, steps')
	plt.xlabel('Time')

	plt.subplot(224)
	plt.plot(m.time, m.accel, 'r-o')
	plt.ylabel('Acceleration, steps/sec^2')
	plt.xlabel('Time')
	plt.show()
