#! /bin/bash

# Auxiliary script to send data to log server using ssh\rsync

display_usage() { 
	echo "This script must be run with one argument:" 
	echo "archive | error | today" 
	} 

if [  $# -ne 1 ]; then 
	display_usage
	exit 1
fi 


server="user@example.com"
server_log_path="/home/user"
server_ssh_port=22

name_from_file="$(grep hostname /home/user/gui/net.txt | cut -f4 -d'"')"
log_dir_name="${name_from_file:-clone}"

dst="$server":"$server_log_path"/"$log_dir_name"/
echo $dst

date_time=$(date +%Y%m%d-%Hh%Mm)
src="/home/user/gui/log/"
ssh_param_common="$server_ssh_port -i /home/user/.ssh/user -o StrictHostKeyChecking=no"


case "${1}" in 
archive) 
    ssh_param="ssh -p $ssh_param_common"
    rsync_log="$src"rsync.log

    echo "==========================" >> "$rsync_log"
    echo $(date) >> "$rsync_log"
    rsync -avtr -e  "$ssh_param" --include '*.gz' --exclude '*' "$src" "$dst" >> "$rsync_log"
;;

error)
    lines_number=500
    file_G="error_G_${date_time}.txt.gz"
    file_B="error_B_${date_time}.txt.gz"

    cd "$src" || exit 1

    tail -$lines_number logT.txt  | gzip -9 -c - >> "$file_B" 
    tail -$lines_number logG.txt  | gzip -9 -c - >> "$file_G"

    scp -P $ssh_param_common "$file_B" "$file_G"  "$dst"

    rm "$file_B" "$file_G"
;;

today) 
    ssh_param='ssh -p $ssh_param_common'
    rsync_log="$src"rsync.log

    echo "==========================" >> "$rsync_log"
    echo $(date) >> "$rsync_log"

    cd "$src" || exit 1
    tar -capf today.tgz *txt --warning=no-file-changed
    rsync -avtr -e "$ssh_param" --include 'today.tgz' --exclude '*' "$src" "$dst" >> "$rsync_log"
;;

*)
    echo "Unknown argument"
;;

esac


