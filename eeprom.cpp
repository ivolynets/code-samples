#include "eeprom.h"

# High lever driver to work with EEEPROM

# write parameter of type T to EEPROM
template <typename T> bool parameter_set(u16 eeprom_addr, T parameter){
	u8 status = ERROR_FUNCTION_PARAMETER;
	size_t parameter_len = sizeof(parameter);
	_info_no_nl("Save");
	_debug_text_val_no_nl_cont(" ", parameter);
	_debug_text_val_no_nl_cont(" to EEPROM, addr ", eeprom_addr);
	_debug_text_val_no_nl_last(", len in bytes = ", parameter_len);
	if (eeprom_addr <= EEPROM.length()-parameter_len) {
		T parameter_eeprom;
		eeprom_update_block (reinterpret_cast<const void *>(&parameter),
							 reinterpret_cast<      void *>(eeprom_addr),
							 parameter_len);
		eeprom_busy_wait();
		parameter_get(eeprom_addr, parameter_eeprom);
		if (parameter == parameter_eeprom)
		{
			status = STATUS_OK;
			_info("EEPROM save ok");
		}
		else
		{
			status = ERROR_EEPROM_WRITE;
			_error_text_val("EEPROM: readed value = ", parameter_eeprom);
		}

	}
	else {
		status = ERROR_FUNCTION_PARAMETER;
		_error_text_val("EEPROM: incorrect addr ", eeprom_addr);
	}
	return check_status (status);
}

# read parameter from EEPROM
template <typename T> void parameter_get(u16 eeprom_addr, T &parameter){
	size_t parameter_len = sizeof(T);
	eeprom_read_block (reinterpret_cast<      void *>(&parameter),
					   reinterpret_cast<const void *>(eeprom_addr),
					   parameter_len);
	_debug_no_nl_frst("Readed ");
	_debug_text_val_no_nl_cont(" ", parameter);
	_debug_text_val_no_nl_cont(" from EEPROM addr ", eeprom_addr);
	_debug_text_val_no_nl_last(", len=", parameter_len);
}
