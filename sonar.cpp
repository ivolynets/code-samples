#include "sonar.h"

# Sonar low level driver

Sonar::Sonar(volatile uint8_t *trig_port,
			 uint8_t trig_pin,
			 volatile uint8_t *echo_port,
			 uint8_t echo_pin,
			 u16 thresh,
			 u16 error)
{
	this->trig_port = trig_port;
	this->trig_pin  = trig_pin;
	this->echo_port = echo_port;
	this->echo_pin  = echo_pin;
	this->thresh    = thresh;
	this->error     = error;
}

void inline Sonar::timer_config(){
	Timer5_reset();
	TIMSK5 = _BV(TOIE5);
}

void inline Sonar::timer_start(){
	Timer5_start(TIMER5_PRESCALER_64); // 16e6/64 = 250000, 4 usec resolution, ~1mm in distance (5.3usec=1mm)
}

void inline Sonar::timer_stop() {
	Timer5_stop();
}

void inline Sonar::start_measure(){
	*trig_port &= ~_BV(trig_pin);
	delayMicroseconds(SONAR_TRIG_DELAY_USEC);
	*trig_port |= _BV(trig_pin);
	delayMicroseconds(SONAR_TRIG_DELAY_USEC);
	*trig_port &= ~_BV(trig_pin);
}


int16_t Sonar::get_stable_distance(const u8 number)
{
	int16_t result = SONAR_DISTANCE_ERROR;
	u16 distance [number];
	const u8 last_index=membersof(distance) - 1;
//	_debug_text_val("Get stable sonar data, times: ", number);
//	_debug_no_nl_frst       ("Sonar data: ");

	_debug_no_nl_cont(" [");
	// get distance 3 times
	for (u8 i=0; i<=last_index ; ++i) {
		distance[i] = get_distance();
		_debug_text_val_no_nl_cont(" ", distance[i]);
	}
	_debug_no_nl_cont(" ] ");

	ulong time_start = millis();
	while (millis()-time_start <= SONAR_TIME_MAX_STABLE_MSEC)
	{
		delay(SONAR_TIME_BETWEEN_ATTEMPTS_MSEC);
		// get 4-th distance
		int16_t cur_distance = get_distance();
		if (cur_distance>0) {
			u16 min_distance=cur_distance;
			u16 max_distance=cur_distance;
			_debug_text_val_no_nl_cont("new=",cur_distance);
			_debug_no_nl_cont(", [");
			for (u8 i=0; i<=last_index ; ++i) {
				distance[i] = (last_index == i) ? cur_distance : distance[i+1];;
				min_distance = (distance[i] < min_distance) ? distance[i] : min_distance;
				max_distance = (distance[i] > max_distance) ? distance[i] : max_distance;
				_debug_text_val_no_nl_cont(" ", distance[i]);
			}
			_debug_no_nl_cont(" ]");
			u16 avg_distance = (static_cast<uint32_t>(min_distance) + max_distance) / 2;
			u16 diff_distance = abs(max_distance - min_distance);

			_debug_text_val_no_nl_cont(" min=", min_distance);
			_debug_text_val_no_nl_cont(" max=", max_distance);
			_debug_text_val_no_nl_cont(" avg=", avg_distance);
			_debug_text_val_no_nl_last	  (" max_diff=", diff_distance);
			if (SONAR_DISTANCE_MAX_STABLE_MM<diff_distance)
			{
				_warning("Too different data");
				continue;
			}
			else
			{
				_info_text_val("Average distance=", avg_distance);
				result = avg_distance;
				break;
			}
		}
		else {
			_error_text_val("Sonar error: ", cur_distance);
		}
	}
	return result;
}

int16_t Sonar::get_distance(){
	bool     status  = false;
	uint32_t result  = 0;
	bool     echo[2] = {0, 0}; // [0] = pevious value, [1] = current
	// clear aux variables
	Timer5_reset();
	start_measure();

	ulong time_start = millis();
	while (millis()-time_start <= SONAR_TIME_MAX_MSEC) {
		echo[0] = echo[1];
		echo[1] = *echo_port & _BV(echo_pin);
		u8 tmp = (u8)((echo[0]<<1) + echo[1]);
		switch (tmp) {
		case 1:	// _____|^^^^^^^  raising edge
			timer_start();
			break;
		case 2:{ // ^^^^^^^|_____ falling edge
			result = TCNT5;
			Timer5_reset();
//			_debug_text_val_no_nl_cont(" /cnt=", result);
			result = (result*USEC_IN_TIMER_TICKS*10)/SONAR_COEF;
//			_debug_text_val_no_nl_cont("; ", result);
//			_debug_no_nl_cont("/ ");
			status = true;
			break;
		}
		default:
			break;
		}
		//		if (status) {
		//			break;
		//		}
	}
	Timer5_reset();
	if (!status){
		result = SONAR_DISTANCE_ERROR;
		_error_text_val("Sonar timeout, ms: ", SONAR_TIME_MAX_MSEC);
	}

	return static_cast<int16_t>(result);
}


bool Sonar::check_object_in_distance() {
	bool status = false;
	u16 min = thresh - error;
	u16 max = thresh + error;
	int16_t distance = get_stable_distance ();
	if (SONAR_DISTANCE_ERROR == distance) {
		_error("Sonar error!");
		check_status(ERROR_SONAR_TIMEOUT);
	}
	else {
//		_debug_text_val_no_nl_continue(" distance=", distance);
		status = ((min<=distance) && (distance<=max)) ? 1 : 0;
	}
	return status;
}


